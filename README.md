# mentorship.kde.org

This is the git repository for [mentorship.kde.org](https://mentorship.kde.org), the website for the KDE mentorship projects.

As a (Hu)Go module, it requires both Hugo and Go to work.

### Development
Read about the shared theme at [hugo-kde wiki](https://invent.kde.org/websites/hugo-kde/-/wikis/)

To build: `just && hugo -e development`

### I18n
See [hugoi18n](https://invent.kde.org/websites/hugo-i18n)

